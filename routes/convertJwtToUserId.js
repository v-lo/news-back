const jwt = require('jsonwebtoken');

module.exports = function (token){
    const userIdDecoded = jwt.verify(token, process.env.TOKEN_SECRET);
    return userIdDecoded;
}