const router = require('express').Router();
const User = require ('../model/User');
const jwt = require('jsonwebtoken');
const bcrypt = require ('bcryptjs');
const {registerValidation, loginValidation} = require('../validation');

//Register
router.post('/register', async (req, res) =>{
    //console.log(req.body)
    console.log('/register sollicited');
    //Validation before creating user
    const {error} = registerValidation(req.body);
    if(error){
        return res.status(400).send(error.details[0].message);
    }

    //Checking if user is already in DB
    const emailExist = await User.findOne({email: req.body.email});
    if(emailExist){
        return res.status(400).send('Email already exists in database');
    }

    //Hash the password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    //Create new user
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword
    });
    try{
        const savedUser = await user.save();
        //res.send({user: user.__id});
        res.send(savedUser);
    }catch(err){
        res.status(400).send(err);
    }
});

//Login
router.post('/login', async (req, res) =>{
    console.log('/login sollicited');

    //Validation before test user integrity
    const {error} = loginValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    //Checking if the email exists
    const user = await User.findOne({email: req.body.email});
    if(!user) return res.status(400).send('Email or password is wrong');//EMAIL DOESN'T EXISTS

    //Checking password
    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if(!validPassword) return res.status(400).send('Email or password is wrong');//EMAIL OR PASSWORD IS WRONG
    
    //Create and asign a token
    const token = jwt.sign({__id: user.id}, process.env.TOKEN_SECRET);
    res.header('auth-token', token).status(200).send({jwt: token, name: user.name});
});


module.exports = router;