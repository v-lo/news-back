const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
//Import Routes
const authRoute = require ('./routes/auth');


dotenv.config();

//DB connect
mongoose.connect(
    process.env.DB_CONNECT,
    { useUnifiedTopology: true },
    () => console.log('connexion to DB OK')
);


//Middlewares
app.use(express.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, auth-token");//Add "auth-token" value for jwt verify !
    next();
});
//Routes MiddleWares (prefixes)
app.use('/api/auth', authRoute);



app.listen (8080, () => console.log('Sever up and listening 8080'));